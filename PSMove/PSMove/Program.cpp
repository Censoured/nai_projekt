#include "Program.h"

Program::Program() {}

Program::~Program() {}

bool Program::init(VideoCapture cap) {
	_camera = cap;
	if (!_camera.isOpened()) {
		return -1;
	}
	_pattern = imread("pattern.png", CV_LOAD_IMAGE_COLOR);
	if (_pattern.data == NULL) {
		return -1;
	}
}

void Program::run() {
	while (_running) {
		handleEvents();
		update();
		render();
	}
}

void Program::focusHSVColor(Mat & source, HSVRange color, Mat & dest) {
	inRange(source, Scalar(color.lowH, color.lowS, color.lowV), Scalar(color.hiH, color.hiS, color.hiV), dest);
}

void Program::createColorTrackbar() {
	namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"
	cvCreateTrackbar("LowH", "Control", &_trackedColor.lowH, 179); //Hue (0 - 179)
	cvCreateTrackbar("HighH", "Control", &_trackedColor.hiH, 255);

	cvCreateTrackbar("LowS", "Control", &_trackedColor.lowS, 255); //Saturation (0 - 255)
	cvCreateTrackbar("HighS", "Control", &_trackedColor.hiS, 255);

	cvCreateTrackbar("LowV", "Control", &_trackedColor.lowV, 255); //Value (0 - 255)
	cvCreateTrackbar("HighV", "Control", &_trackedColor.hiV, 255);
}

void Program::matchTemplate() {
	Mat img_display;
	Mat _matchingResult;
	_motionTrack.copyTo(img_display);


	/// Create the result matrix
	int result_cols = _motionTrack.cols - _pattern.cols + 1;
	int result_rows = _motionTrack.rows - _pattern.rows + 1;

	_matchingResult.create(result_rows, result_cols, CV_32FC1);
	/// Do the Matching and Normalize
	cv::matchTemplate(_motionTrack, _pattern, _matchingResult, _matchingAlgorithm);
	normalize(_matchingResult, _matchingResult, 0, 1, NORM_MINMAX, -1, Mat());

	/// Localizing the best match with minMaxLoc
	minMaxLoc(_matchingResult, &_minValue, &_maxValue, &_minLocation, &_maxLocation, Mat());

	/// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all the other methods, the higher the better
	if (_matchingAlgorithm == CV_TM_SQDIFF || _matchingAlgorithm == CV_TM_SQDIFF_NORMED) {
		_matchLocation = _minLocation;
	} else {
		_matchLocation = _maxLocation;
	}

	/// Show me what you got
	rectangle(img_display, _matchLocation, Point(_matchLocation.x + _pattern.cols, _matchLocation.y + _pattern.rows), Scalar(0xFF, 0xFF, 0, 0), 2, 8, 0);
	rectangle(_matchingResult, _matchLocation, Point(_matchLocation.x + _pattern.cols, _matchLocation.y + _pattern.rows), Scalar::all(0), 2, 8, 0);

	//imshow("Object Tracking", img_display);
	//imshow("Result", result);
}

void Program::handleEvents() {}

void Program::update() {}

void Program::render() {
	imshow("Camera preview", _frame);
	imshow("Color Mask", _maskedFrame);
	imshow("Object Tracking", _motionTrack);
	imshow("Pattern", _pattern);
}
