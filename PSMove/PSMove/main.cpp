
#include <iostream>

#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\flann\timer.h>
#include <Windows.h>

using namespace cv;
using namespace std;

struct HSVRange {
	int lowH;
	int lowS;
	int lowV;

	int hiH;
	int hiS;
	int hiV;
};

#define TEMPLATE_COUNT 4

Mat frame;
Mat lines;
Mat templates[TEMPLATE_COUNT];
Mat diff;
Mat processedFrame;
Mat compareImg;
Mat exitFile;

int dump = 1;

VideoCapture cap;

cvflann::StartStopTimer timer;

Rect drawingCopy;

bool _running = true;

HSVRange _trackedColor = { 70, 66, 230, 90, 255, 255 };
HSVRange whiteColor = { 255, 255, 255, 255, 255, 255 };

void MatchingMethod();
void track(const Mat source, Mat &dest, int &x, int &y);
void focusHSVColor(Mat &source, HSVRange color, Mat &dest) {
	inRange(source, Scalar(color.lowH, color.lowS, color.lowV), Scalar(color.hiH, color.hiS, color.hiV), dest);
}

void prepareMats() {
	for (int i = 1; i <= TEMPLATE_COUNT; i++) {
		templates[i-1] = imread("templates\\"+std::to_string(i)+".png", CV_LOAD_IMAGE_COLOR);
	}
	

	cap.read(frame);
	diff = Mat::zeros(templates[0].size(), CV_8UC3);
	exitFile = Mat::zeros(templates[0].size(), CV_8UC3);
	lines = Mat::zeros(frame.size(), CV_8UC3);
}

void createWindows() {
	namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"
	cvCreateTrackbar("LowH", "Control", &_trackedColor.lowH, 179); //Hue (0 - 179)
	cvCreateTrackbar("HighH", "Control", &_trackedColor.hiH, 255);

	cvCreateTrackbar("LowS", "Control", &_trackedColor.lowS, 255); //Saturation (0 - 255)
	cvCreateTrackbar("HighS", "Control", &_trackedColor.hiS, 255);

	cvCreateTrackbar("LowV", "Control", &_trackedColor.lowV, 255); //Value (0 - 255)
	cvCreateTrackbar("HighV", "Control", &_trackedColor.hiV, 255);
}

void showWindows() {
	//imshow("Camera preview", frame);
	imshow("Color Mask", processedFrame);

	imshow("Object Tracking", lines);
	//imshow("Pattern", templates);
}

void handleInput() {
	switch (waitKey(5)) {
	case 13:
		lines = Mat::zeros(lines.size(), CV_8UC3);
		break;
	case 27:
		_running = false;
		break;
	case 'm':
		imwrite("dump"+std::to_string(dump)+".png", compareImg);
		dump++;
		lines = Mat::zeros(lines.size(), CV_8UC3);
		break;
	default:
		break;
	}
	timer.stop();
	if (timer.value >= 3000) {
		timer.reset();
		lines = Mat::zeros(lines.size(), CV_8UC3);
	}
}

void getContour(Mat source) {

	Mat canny_output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	int thresh = 100;

	Canny(source, canny_output, thresh, thresh * 2, 3);

	findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	vector<vector<Point> > contours_poly(contours.size());
	vector<Rect> boundRect(contours.size());
	vector<Point2f>center(contours.size());
	vector<float>radius(contours.size());

	if(contours.size() !=0) {
		approxPolyDP( Mat(contours[0]), contours_poly[0], 3, true );
		boundRect[0] = boundingRect(Mat(contours_poly[0]));
		drawingCopy = Rect(boundRect[0].tl(), boundRect[0].br());
	}
}

int main() {
#pragma region init
	cap.open(0);
	if (!cap.isOpened()) {
		return -1;
	}
	prepareMats();

	createWindows();
	
	int x = -1;
	int y = -1;

	timer.reset();
#pragma endregion
	while (_running) {

		system("cls");
		timer.start();
		cap >> frame;
		flip(frame, frame, 1);
		
		cvtColor(frame, processedFrame, CV_BGR2HSV);
		focusHSVColor(processedFrame, _trackedColor, processedFrame);
		
		track(processedFrame, lines, x, y);

		getContour(lines);
		
		exitFile = lines(drawingCopy).clone();
		
		if(drawingCopy.height != 0 && drawingCopy.width != 0) {
			resize(exitFile, compareImg, Size(templates[0].cols, templates[0].rows), 0, 0, INTER_LINEAR);
			for (int i = 0; i < TEMPLATE_COUNT; i++) {
				cv::compare(compareImg, templates[i], diff, cv::CMP_NE);
				cvtColor(diff, diff, CV_RGB2GRAY);
				imshow("Compare"+i, diff);
				int a = countNonZero(diff);
				printf("Template[%d] = %d\n", i, a);
				if (a < 1000) {
					printf("Matched! %d\n", a);
					//lines = Mat::zeros(frame.size(), CV_8UC3);
				}
			}
		}
		

		handleInput();
		showWindows();
	}
	return 0;
}

void track(const Mat source, Mat &dest, int &x, int &y) {

	Moments oMoments = moments(source);

	double dM01 = oMoments.m01;
	double dM10 = oMoments.m10;
	double dArea = oMoments.m00;

	if (dArea > 10000) {
		int posX = dM10 / dArea;
		int posY = dM01 / dArea;

		if (x >= 0 && y >= 0 && posX >= 0 && posY >= 0) {
			if (x != posX || y != posY) {
				line(dest, Point(posX, posY), Point(x, y), Scalar(255, 255, 255), 20);
			}
		}
		x = posX;
		y = posY;

	}
}