#pragma once

#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\flann\timer.h>

using namespace cv;
using namespace std;

//#define SHOW_MATCHING_RESULT

struct HSVRange {
	int lowH;
	int lowS;
	int lowV;

	int hiH;
	int hiS;
	int hiV;
};

class Program {
public:
	Program();
	~Program();
	bool init(VideoCapture cap);
	void run();
private:
	VideoCapture _camera;
	bool _running = true;
	//image matrices
	Mat _frame; //unprocessed dump from camera
	Mat _maskedFrame; //processed frame to react for exact color
	Mat _pattern; //image of gesture to recognize
	Mat _motionTrack;

	HSVRange _trackedColor;
	//matching properties
	int _matchingAlgorithm = 0;
	double _minValue = 0;
	double _maxValue = 0;

	Point _minLocation;
	Point _maxLocation;
	Point _matchLocation;

	void focusHSVColor(Mat &source, HSVRange color, Mat &dest);

	void createColorTrackbar();
	void matchTemplate();

	void handleEvents();
	void update();
	void render();
};